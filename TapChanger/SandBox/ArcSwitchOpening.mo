within TapChanger.SandBox;
model ArcSwitchOpening "Demonstrating the opening of a swith with arc"
  extends Modelica.Icons.Example;
  Modelica.Electrical.Analog.Ideal.OpenerWithArc switch(
    V0=50,
    dVdt=200,
    Vmax=300) annotation (Placement(transformation(extent={{-40,10},{-20,30}})));
  Modelica.Electrical.Analog.Basic.Inductor inductor(L=1) annotation (Placement(transformation(extent={{0,10},{20,30}})));
  Modelica.Electrical.Analog.Basic.Ground ground annotation (Placement(transformation(extent={{-70,-40},{-50,-20}})));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V=200) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-60,10})));
  Modelica.Blocks.Sources.BooleanStep booleanStep(startTime=0.5) annotation (Placement(transformation(extent={{-80,40},{-60,60}})));
  Modelica.Electrical.Analog.Basic.Resistor resistor(R=10) annotation (Placement(transformation(
        extent={{-9.5,-9.5},{9.5,9.5}},
        rotation=270,
        origin={39.5,0.5})));
equation
  connect(constantVoltage.n, ground.p) annotation (Line(points={{-60,0},{-60,-20},{-60,-20}}, color={0,0,255}));
  connect(constantVoltage.p, switch.p) annotation (Line(points={{-60,20},{-40,20}}, color={0,0,255}));
  connect(switch.n, inductor.p) annotation (Line(points={{-20,20},{0,20}}, color={0,0,255}));
  connect(booleanStep.y, switch.control) annotation (Line(points={{-59,50},{-30,50},{-30,31}}, color={255,0,255}));
  connect(inductor.n, resistor.p) annotation (Line(points={{20,20},{39.5,20},{39.5,10}}, color={0,0,255}));
  connect(resistor.n, ground.p) annotation (Line(points={{39.5,-9},{39.5,-10},{40,-10},{40,-20},{-60,-20}}, color={0,0,255}));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio=false)),
    Diagram(coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=2));
end ArcSwitchOpening;
