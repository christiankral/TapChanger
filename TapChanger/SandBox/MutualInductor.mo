within TapChanger.SandBox;
model MutualInductor
  extends Modelica.Icons.Example;
  Modelica.Electrical.Analog.Basic.M_Transformer transformer(L = {1, 1, 1}, N = 2)  annotation (
      Placement(visible = true, transformation(origin = {-16, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation (
      Placement(visible = true, transformation(origin = {-60, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.SineVoltage sineVoltage(V = 100, freqHz = 50)  annotation (
      Placement(visible = true, transformation(origin = {-60, 10}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
equation
    connect(ground.p, sineVoltage.n) annotation (
      Line(points = {{-60, -20}, {-60, -20}, {-60, 0}, {-60, 0}}, color = {0, 0, 255}));
  connect(sineVoltage.p, transformer.p[1]) annotation (
      Line(points={{-60,20},{-60,20},{-60,34},{-26,34},{-26,30.5}},          color = {0, 0, 255}));
  connect(transformer.n[1], transformer.p[2]) annotation (
      Line(points={{-6,30.5},{10,30.5},{10,20},{-40,20},{-40,32},{-26,32},{-26,37.5}},          color = {0, 0, 255}, thickness = 0.5));
  connect(transformer.n[2], ground.p) annotation (
      Line(points={{-6,37.5},{18,37.5},{18,-20},{-60,-20},{-60,-20}},        color = {0, 0, 255}));
end MutualInductor;
