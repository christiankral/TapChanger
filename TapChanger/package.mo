within ;
package TapChanger "Tap changer for power transformers"
  extends Modelica.Icons.Package;

  annotation (uses(Modelica(version="3.2.3")));
end TapChanger;
